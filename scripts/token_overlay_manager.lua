OOB_MSGTYPE_DELETE_ALL_WOUND_OVERLAYS = "delete_all_wound_overlays";

function onInit()
	UDGCoreRPGWoundOverlayHelper.verbose({"token_overlay_manager.lua::onInit()"});

	UDGCoreRPGWoundOverlayHelper.debug("Ruleset: ", Session.RulesetName);

	if Session.IsHost then
		Comm.registerSlashHandler("clearWoundOverlays", sendMessageDeleteAllWoundOverlays);
	end

	DB.addHandler("combattracker.list.*.status", "onUpdate", handleStatusOnUpdate);

	OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_DELETE_ALL_WOUND_OVERLAYS, handleMessageDeleteAllWoundOverlays);
end

function handleStatusOnUpdate(statusNode)
	UDGCoreRPGWoundOverlayHelper.verbose({"token_overlay_manager.lua::handleStatusOnUpdate(statusNode)", "statusNode: ", statusNode, "status Value: ", statusNode.getValue()});
	local nodeCT = statusNode.getParent();
	UDGCoreRPGWoundOverlayTokenOverlayManager.updateWoundOverlay(nodeCT);
end

function updateWoundOverlay(nodeCT)
	UDGCoreRPGWoundOverlayHelper.verbose({"token_overlay_manager.lua::updateWoundOverlay(statusNode)", "nodeCT: ", nodeCT});

	local tokenCT = CombatManager.getTokenFromCT(nodeCT);

	local status = nodeCT.getChild("status").getValue();
	UDGCoreRPGWoundOverlayHelper.verbose({"token_overlay_manager.lua::updateWoundOverlay original status:", status});

	status = translateStatus(status);
	UDGCoreRPGWoundOverlayHelper.verbose({"token_overlay_manager.lua::updateWoundOverlay translated status:", status});

	if not status then
		return;
	end

	if tokenCT then
		local nDU = GameSystem.getDistanceUnitsPerGrid();
		local nNodeSpace = DB.getValue(nodeCT, "space", nDU);
		if nNodeSpace == 0 then
			nNodeSpace = .1
		end
		local nSpace = math.ceil(nNodeSpace / nDU)*100;
		local tokenWidth = nSpace;
		local tokenHeight = nSpace;

		local woundWidget = tokenCT.findWidget("wound");
		if woundWidget then woundWidget.destroy(); end

		woundWidget = tokenCT.addBitmapWidget();
		woundWidget.setName("wound");
		woundWidget.setSize(math.floor(tokenWidth*1), math.floor(tokenHeight*1));
		if status == "LIGHT" then
			woundWidget.setBitmap("overlay_light");
		elseif status == "MODERATE" then
			woundWidget.setBitmap("overlay_moderate");
		elseif status == "WOUNDED" then
			woundWidget.setBitmap("overlay_wounded");
		elseif status == "HEAVY" then
			woundWidget.setBitmap("overlay_heavy");
		elseif status == "CRITICAL" then
			woundWidget.setBitmap("overlay_critical");
		elseif status == "DYING" then
			woundWidget.setBitmap("overlay_dying");
		elseif status == "DEAD" then
			woundWidget.setBitmap("overlay_death");
		end
		woundWidget.bringToFront();
	else
		UDGCoreRPGWoundOverlayHelper.verbose({"No token associated with nodeCT", nodeCT});
	end
end

function translateStatus(status)
	UDGCoreRPGWoundOverlayHelper.verbose({"token_overlay_manager.lua::translateStatus(status)", "status ", status});

	if status == "Healthy" then
		return "HEALTHY";
	elseif status == "Light" or status == "Light Damage" then
		return "LIGHT";
	elseif status == "Moderate" or status == "Medium Damage" then
		return "MODERATE";
	elseif status == "Wounded" then
		return "WOUNDED";
	elseif status == "Heavy" or status == "Severe Damage" then
		return "HEAVY";
	elseif status == "Critical" or status == "Critical Damage" then
		return "CRITICAL";
	elseif string.match(status, "Dying") or string.match(status, "Disabled") then
		return "DYING";
	elseif string.match(status, "Dead") then
		return "DEAD";
	end
end

function updateAllWoundOverlays()
	UDGCoreRPGWoundOverlayHelper.verbose({"token_overlay_manager.lua::updateAllWoundOverlays()"});
	local combatants = CombatManager.getCombatantNodes();
	for _,combatant in pairs(combatants) do
		updateWoundOverlay(combatant);
	end
end

function handleMessageDeleteAllWoundOverlays(msgOOB)
	UDGCoreRPGWoundOverlayHelper.verbose({"token_overlay_manager.lua::handleMessageDeleteAllWoundOverlays(msgOOB)", "msgOOB:", msgOOB});
	local aEntries = CombatManager.getSortedCombatantList();

	for _, node in ipairs(aEntries) do
		local token = CombatManager.getTokenFromCT(node);

		if token then
			woundWidget = token.findWidget("wound");
			if woundWidget then 
				woundWidget.setVisible(false);
				woundWidget.destroy(); 
			end
		end

	end
end

function sendMessageDeleteAllWoundOverlays()
	UDGCoreRPGWoundOverlayHelper.verbose({"token_overlay_manager.lua::sendMessageDeleteAllWoundOverlays()"});
	local msgOOB = {};
	msgOOB.type = OOB_MSGTYPE_DELETE_ALL_WOUND_OVERLAYS;
	Comm.deliverOOBMessage(msgOOB);
end
