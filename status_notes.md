## 3.5 
### Simple Wound Categories
- Healthy
- Wounded
- Heavy
- Disabled

### Detailed Wound Categories
- Light
- Moderate
- Heavy
- Critical
- Disabled

## 4E
### Simple Wound Categories
?

### Detailed Wound Categories
?

## 5E
### Simple Wound Categories
- Healthy
- Wounded
- Heavy
- Dead

### Detailed Wound Categories
- Healthy
- Light
- Moderate
- Heavy
- Critical
- Dead

## Cypher System
### Simple Wound Categories
?
### Detailed Wound Categories
?

## Fate Core
### Simple Wound Categories
?
### Detailed Wound Categories
?

## Numenera
### Simple Wound Categories
?
### Detailed Wound Categories
?

## PFRPG
### Simple Wound Categories
- Healthy
- Wounded
- Heavy
- Disabled
### Detailed Wound Categories
- Light
- Moderate
- Heavy
- Critical
- Disabled

## PFRPG2
### Simple Wound Categories
- Healthy
- Wounded
- Heavy
- Dying
### Detailed Wound Categories
- Light
- Moderate
- Heavy
- Critical
- Dying

## RoleMasterClassic
## Simple Wound Categories
- Healthy
- Light Damage
- Medium Damage
- Severe Damage
- Critical Damage
- Unconscious or Dead
### Detailed Wound Categories
- Healthy
- Light Damage
- Medium Damage
- Severe Damage
- Critical Damage
- Unconscious or Dead

## SFRPG
### Simple Wound Categories
?
### Detailed Wound Categories
?

The Strange
Simple Wound Categories
?
Detailed Wound Categories
?
