# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.0.11]
- Added PF@ Legacy as an allowed ruleset.

## [1.0.10]

- Deleted unnecessary HEALTHY line when setting widgets
- Added a bit of debugging to assist with rulesets I don't own

## [1.0.9] - 2021-06-24

### Added

- Fixed issue with wound widgets on creatues with a space of 0

## [1.0.8] - 2021-03-01

### Added

- Added slash command /clearWoundOverlays
- Removed calls to User.isHost()
- Extension announcement text
- Added all CoreRPG rulesets as allowable
- Added status translation layer

## [1.0.7] - 2021-02-24

### Changed

- Adjusted positioning of clear wounds button so overlapping won't happen

## [1.0.6] - 2021-02-23

### Changed

- Renamed script imports to avoid collisions with other extensions

## [1.0.5] - 2021-02-23

### Changed

- Getting rid of chat command to clear wound overlays

## [1.0.4] - 2021-02-22

### Added

- Works for RolemasterClassic ruleset

## [1.0.3] - 2021-02-22

### Changed

- Updated light wound overlay bitmap to be a bit less bloody

## [1.0.2] - 2021-02-22

### Added

- Fixed up which icons were used for which status
- Added icon for light status
- Removed unused icons from repository
- Removed unused lua file

## [1.0.1] - 2021-02-21

### Added

- Fixed issue with Clear Wound Overlay button overlapping the Zoom To Fit button

## [1.0.0] - 2021-02-21

### Added

- Wound overlays to tokens based on health status
